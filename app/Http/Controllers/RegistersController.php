<?php

namespace App\Http\Controllers;

use App\Models\Registers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Hash;



class RegistersController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin');
    }

    public function getRegisters(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
        $min_age = $request->get('min');
        $min_age = 0;

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = Registers::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Registers::select('count(*) as allcount')->where('name', 'like', '%' . $searchValue . '%')->count();

        // Fetch records
        $records = Registers::orderBy($columnName, $columnSortOrder)
            ->where('company_name', 'like', '%' . $searchValue . '%')
            ->orWhere('company_code', 'like', '%' . $searchValue . '%')
            ->orWhere('name', 'like', '%' . $searchValue . '%')
            ->orWhere('email', 'like', '%' . $searchValue . '%')
            ->skip($start)
            ->take($rowperpage)
            ->get();

        $data_arr = array();
        $sno = $start + 1;
        foreach ($records as $record) {
            $id = $record->id;
            $company_name = $record->company_name;
            $company_code = $record->company_code;
            $name = $record->name;
            $age = $record->age;
            $email = $record->email;
            $button = '<a class="btn btn-primary" href="' . route('admin.edit', $id) . '">Muuda</a>';

            $data_arr[] = array(
                "id" => $id,
                "company_name" => $company_name,
                "company_code" => $company_code,
                "name" => $name,
                "age" => $age,
                "email" => $email,
                "button" => $button
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        echo json_encode($response);
        exit;
    }

    public function apiRegister(Request $request) {

        $r = json_decode($request->data);

        $rules = array(
            'name'       => 'required|min:3|max:255',
            'email'      => 'required|email|unique:registers,email',
            'age' => 'required|date',
            'password' => 'min:6'
        );

        if ($r->company) {
            $rules['company_name'] = 'required';
            $rules['company_code'] = 'required|max:10';
        }

        $validator = Validator::make((array) $r, $rules);

        if ($validator->fails()) {

                return response()->json([
                    "errors" => $validator->errors()
                ], 201);

        } else {

            $register = new Registers;
            if ($r->company_name != '') $register->company_name = $r->company_name;
            if ($r->company_code != '') $register->company_code = $r->company_code;
            if ($r->name != '') $register->name = $r->name;
            if ($r->email != '') $register->email = $r->email; 
            if ($r->age != '') $register->age = $r->age;
            if ($r->password != '') $register->password = $r->password;

            $register->save();

            return response()->json([
                "message" => "Registreeritud"
            ], 201);

        }



    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //TODO: Add users from admin panel
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the shark
        $register = Registers::find($id);

        // show the edit form and pass the shark
        return View::make('admin.edit')
            ->with('register', $register);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        switch ($request->input('action')) {
            case 'delete':

                $register = Registers::find($id);
                $register->delete();

                return Redirect::to('admin')->with('status', 'Kasutaja kustutatud');

                break;

            case 'save':

                //TODO: Translate validator errors
                $rules = array(
                    'name'       => 'required',
                    'email'      => 'required|email',
                    'age' => 'required|integer'
                );
                $validator = Validator::make($request->all(), $rules);

                if ($validator->fails()) {
                    return Redirect::to('admin/' . $id . '/edit')
                        ->withErrors($validator);
                } else {
                    $register = Registers::find($id);
                    if ($request->get('cname') != '') $register->company_name = $request->get('cname');
                    if ($request->get('code') != '') $register->company_code = $request->get('code');
                    if ($request->get('name') != '') $register->name = $request->get('name');
                    if ($request->get('email') != '') $register->email = $request->get('email');
                    if ($request->get('age') != '') $register->age = $request->get('age');
                    if ($request->get('pass') != '') $register->password = Hash::make($request->get('pass'));

                    $register->save();

                    return Redirect::to('admin')->with('status', 'Kasutaja uuendatud');
                }

                break;

        }
    }
}
