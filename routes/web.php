<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistersController;


Route::get('/', function () {
    return view('app');
});



Route::resource('admin', RegistersController::class);

Route::get('/getdata', [RegistersController::class, 'getRegisters'])->name('admin.getdata');
