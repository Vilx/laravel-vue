<!DOCTYPE html>
<html>
<head>
    <title>Kasutaja muutmine</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <br />
    <br />
    
<a href="{{ URL::to('admin') }}" class="btn btn-primary">Tagasi</a>
<br /><br />

<h1>{{ $register->name }} muutmine</h1>

<br />

@php
   foreach ($errors->all() as $error) {
        echo '<div class="alert alert-danger" role="alert">' . $error . '</div>';
    }
@endphp 


<form method="POST" action="{{ route('admin.update', $register->id) }}">
    @method('PUT')
    @csrf
    <div class="form-floating mb-3">
        <input type="text" id="cname" name="cname" class="form-control" placeholder="Firma nimi" value="{{ $register->company_name }}">
        <label for="name">Firma nimi</label>
    </div>
    <div class="form-floating mb-3">
        <input type="text" id="code" name="code" class="form-control"placeholder="Registrikood" value="{{ $register->company_code }}">
        <label for="name">Registrikood</label>
    </div>
    <div class="form-floating mb-3">
        <input type="text" id="name" name="name" class="form-control"placeholder="Nimi" value="{{ $register->name }}">
        <label for="name">Nimi</label>
    </div>
    <div class="form-floating mb-3">
        <input type="text" id="email" name="email" class="form-control"placeholder="Email" value="{{ $register->email }}">
        <label for="name">Email</label>
    </div>
    <div class="form-floating mb-3">
        <input type="text" id="age" name="age" class="form-control"placeholder="Vanus" value="{{ $register->age }}">
        <label for="name">Vanus</label>
    </div>
    <div class="form-floating mb-4">
        <input type="text" id="pass" name="pass"placeholder="Uus parool" class="form-control">
        <label for="name">Uus parool</label>
    </div>

    <button class="btn btn-primary me-3" type="submit" name="action" value="save">Salvesta kasutaja</button>
    <button class="btn btn-danger" type="submit" name="action" value="delete">Kustuta kasutaja</button>

</form>

</div>

<br />
<br />

</body>
</html>