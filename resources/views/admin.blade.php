<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- jQuery CDN -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
   
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.3.4/css/searchBuilder.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.3.4/js/dataTables.searchBuilder.min.js"></script>
   
</head>
<body>
    <br />
    <br />
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <button id="fbutton" class="btn btn-primary float-right" style="float: right; margin-bottom: 10px;">Veel filtreid</button>

                        <table id="extra-filters" border="0" cellspacing="5" style="display: none; margin-left: auto; margin-bottom: 5px;">
                            <tbody><tr>
                                <td>Minimum age:</td>
                                <td><input style="box-sizing: border-box;
                                    margin: 0;
                                    font-family: inherit;
                                    font-size: inherit;
                                    line-height: inherit;
                                    outline-offset: -2px;
                                    -webkit-appearance: textfield;
                                    border: 1px solid #aaa;
                                    border-radius: 3px;
                                    padding: 5px;
                                    background-color: transparent;
                                    margin-left: 3px;
                                    margin-bottom: 5px" type="text" id="min" name="min"></td>
                            </tr>
                            <tr>
                                <td>Maximum age:</td>
                                <td><input type="text" style="box-sizing: border-box;
                                    margin: 0;
                                    font-family: inherit;
                                    font-size: inherit;
                                    line-height: inherit;
                                    outline-offset: -2px;
                                    -webkit-appearance: textfield;
                                    border: 1px solid #aaa;
                                    border-radius: 3px;
                                    padding: 5px;
                                    background-color: transparent;
                                    margin-left: 3px;" id="max" name="max"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="checkbox" id="company" name="company" value="Company">
                                    <label for="company">&nbsp;&nbsp;Näita ainult firmasid</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <table id='empTable' class="display" width='100%' style='width: 100%;'>
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Firma nimi</td>
                                <td>Registrikood</td>
                                <td>Nimi</td>
                                <td>Vanus</td>
                                <td>Email</td>
                                <td></td>
                            </tr>
                        </thead>
                    </table>

            </div>
        </div>
    </div>
    <script type="text/javascript">


        //TODO: Fix filters other than search
        $.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
           
            if (
                $('#company').is(':checked') && data[2] != '' 
            ) {
                return true;
            }
            return false;
        });

        $.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {

            var min = parseInt($('#min').val(), 10);
            var max = parseInt($('#max').val(), 10);
            var age = parseFloat(data[4]) || 0; // use data for the age column
        
            if (
                (isNaN(min) && isNaN(max)) ||
                (isNaN(min) && age <= max) ||
                (min <= age && isNaN(max)) ||
                (min <= age && age <= max)
            ) {
                return true;
            }
            return false;
        });

        $(document).ready(function(){

            $('#fbutton').click(function(){
                $( "#fbutton" ).toggle();
                $( "#extra-filters" ).toggle();
            });

          //TODO: Translate
          // DataTable
          var table = $('#empTable').DataTable({
             processing: true,
             serverSide: true,
             ajax: "{{route('admin.getdata')}}",
             columns: [
                { data: 'id' },
                { data: 'company_name' },
                { data: 'company_code' },
                { data: 'name' },
                { data: 'age' },
                { data: 'email' },
                { data: 'button' },
             ]
          });


                $('#min, #max').keyup(function () {
                    table.draw();
                });


        });

        
    </script>
    <br />
    <br />
</body>
</html>