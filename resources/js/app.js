
 import Vue from 'vue'

 import store from './Store/index';
 import App from './App.vue'


 const app = new Vue({
    el: '#app',
    store,
    components: { App }
  });

