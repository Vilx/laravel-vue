import Vue from 'vue';
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    registers: [],
    filled: false
  },
  mutations: {
    addRegister: (state, data) => {
      state.registers.push(data);
    },
    addFilled: (state, data) => {
      state.filled = data;
    },
  },
  actions: {
    setRegister: (state, reg) => {
      state.commit('addRegister', reg)
    },
    setFilled: (state, value) => {
      state.commit('addFilled', value)
    },
  },
  getters: {
    getRegisters: (state, getters) => {
      return state.registers;
    },
    getFilled: (state, getters) => {
      return state.filled;
    },
    
  },
  plugins: [createPersistedState({
    paths: ['registers']
  })],
})

export default store;
